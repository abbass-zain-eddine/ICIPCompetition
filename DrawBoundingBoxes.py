import torch
import torchvision.transforms as transforms
import matplotlib.pyplot as plt
import numpy as np
import cv2

class BboxPlot():
    def __init__(self,img,targets,classes):
        self.image=img
        self.targets=targets
        self.classes=classes
    def BboxPlot(self,with_proba=False,show=True,save=False,save_path='',img_name=''):
    
        # Define the transformation to convert PIL images to tensors
        transform = transforms.Compose([transforms.ToTensor()])

        # Convert the PIL image to a tensor
        if not torch.is_tensor(self.image):
            self.image = transform(self.image)
            

        fig, ax = plt.subplots(figsize = (10,12))
        # Plot the image
        ax.imshow(self.image.permute(1, 2, 0))

        # Plot the normalized bounding boxes on the image
        shape=self.image.shape
        if np.shape(self.targets) == 0:
            if with_proba:
                self.targets=[[0,0,0,0,0,0]]
            else:
                self.targets=[[0,0,0,0,0]]

        for target in self.targets:

            if with_proba:
                c,p,x, y, w, h = target
            else:
                c,x, y, w, h = target
            x *= shape[2]
            y *= shape[1]
            w *= shape[2]
            h *= shape[1]
            ax.add_patch(plt.Rectangle((x-w/2, y-h/2), w, h, linewidth=2, edgecolor='r', facecolor='none'))
            if(c.type(torch.int32)!=0):
                ind=c.type(torch.int32)-1
                ax.text(x-w/2,(y-h/2-10),str(self.classes[ind]),verticalalignment='top',color='red',fontsize=12,weight="bold")
            if save:
                plt.savefig(save_path+"/"+img_name)
        if show:
            fig.show()




    def yolov8Plot(self,save_path='',img_name=''):
        for box in self.targets.boxes:
            c, p,x, y, w, h = box
            cv2.rectangle(self.image, (int(x), int(y)), (int(w), int(h)), (255, 255, 0), 2)
            ind=int(c)-1
            cv2.putText(self.image,str(self.classes[ind]),(int(x),int(y-10)),cv2.FONT_HERSHEY_SIMPLEX,1,(255,255,0))
        cv2.imwrite(save_path+"/" + img_name, self.image)



#results = model.predict(source="dataset/IR DATA/test/images")

# results would be a generator which is more friendly to memory by setting stream=True
# 2. return as a generator
# results = model.predict(source=0, stream=True)

# for result in results:
#     # detection
#     result.boxes.xyxy   # box with xyxy format, (N, 4)
#     result.boxes.xywh   # box with xywh format, (N, 4)
#     result.boxes.xyxyn  # box with xyxy format but normalized, (N, 4)
#     result.boxes.xywhn  # box with xywh format but normalized, (N, 4)
#     result.boxes.conf   # confidence score, (N, 1)
#     result.boxes.cls    # cls, (N, 1)

#     # segmentation
#     result.masks.masks     # masks, (N, H, W)
#     result.masks.segments  # bounding coordinates of masks, List[segment] * N

#     # classification
#     result.probs     # cls prob, (num_class, )

# # Each result is composed of torch.Tensor by default, 
# # in which you can easily use following functionality:
# result = result.cuda()
# result = result.cpu()
# result = result.to("cpu")
# result = result.numpy()