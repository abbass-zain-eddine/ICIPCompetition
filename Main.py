from DrawBoundingBoxes import BboxPlot
from  DataLoad import DCCOCODataset
from torch.utils.data import DataLoader
from torchvision import transforms
import glob
from DataPreprocessing import Preprocessing
import os,numpy as np
import matplotlib.pyplot as plt
# Example usage
mainpath="/home/zeineddine/ICIPCompet/DCCOCO/train"
image_paths = glob.glob(os.path.join(mainpath,"images/*.jpg"))
bbox_paths = glob.glob(os.path.join(mainpath,"labels/*.txt"))
classes=['person','bicycle','car','motorcycle','airplane','bus','train','truck','boat','traffic light','fire hydrant','street sign'\
         ,'stop sign','parking meter','bench','bird','cat','dog','horse','sheep','cow','elephant','bear','zebra','giraffe','hat',
'backpack','umbrella','shoe','eye glasses','handbag','tie','suitcase','frisbee','skis','snowboard','sports ball','kite'\
    ,'baseball bat','baseball glove','skateboard','surfboard','tennis racket','bottle','plate','wine glass','cup','fork','knife'\
        ,'spoon','bowl','banana','apple','sandwich','orange','broccoli','carrot','hot dog','pizza','donut','cake','chair','couch'\
            ,'potted plant','bed','mirror','dining table','window','desk','toilet','door','tv','laptop','mouse','remote','keyboard'\
                ,'cell phone','microwave','oven','toaster','sink','refrigerator','blender','book','clock''vase','scissors','teddy bear'\
                    ,'hair drier','toothbrush','hair brush']


resampling_weights={1: 0.25, 2: 1, 3: 0.15, 4: 1, 5: 1, 6: 1, 7: 1, 8: 1, 9: 1, 10: 1, 11: 2\
 , 12: 0, 13: 2, 14: 2, 15: 1, 16: 1, 17: 1, 18: 1, 19: 1, 20: 1, 21: 1, 22: 1\
    , 23: 2, 24: 1, 25: 1, 26: 0, 27: 1, 28: 1, 29: 0, 30: 0, 31: 1, 32: 1, 33: 1, 34: 2\
        , 35: 1, 36: 2, 37: 1, 38: 1, 39: 2, 40: 1.5, 41: 1, 42: 1, 43: 1, 44: 0.5, 45: 0\
            , 46: 1, 47: 1, 48: 1, 49: 1, 50: 1, 51: 0.8, 52: 1, 53: 1, 54: 1.2, 55: 1, 56: 1\
                , 57: 1, 58: 2, 59: 1, 60: 1, 61: 1, 62: 0.5, 63: 1, 64: 1, 65: 1.2, 66: 0, 67: 0.8\
                    , 68: 0, 69: 0, 70: 1.2, 71: 0, 72: 1, 73: 1.1, 74: 2, 75: 1, 76: 2, 77: 1, 78: 2\
                        , 79: 2, 80: 3, 81: 1.1, 82: 2, 83: 0, 84: 0.5, 85: 1, 86: 1, 87: 2, 88: 1.2\
                            , 89: 3, 90: 2}
# Define a transform to resize the image and normalize pixel values
transform = transforms.Compose([
    transforms.Resize((640, 640)),
    transforms.ToTensor(),
    #transforms.Normalize(mean=[0.485, 0.456, 0.406], std=[0.229, 0.224, 0.225])
])
### Create a dataset object
# dataset = DCCOCODataset(image_paths, bbox_paths, transform=transform)
### Create a data loader to load the dataset in batches
# batch_size = 2
# data_loader = DataLoader(dataset, batch_size=batch_size, shuffle=True,collate_fn=dataset.collate_fn)

###plot images annotated from data_loader
# for image,boxes in data_loader:
#     BboxPlot(image[0],boxes[0],classes).BboxPlot()


###Test dataset.get_images_with_no_labels function from class DCCOCODataset
#dataset.get_images_with_no_labels("/home/zeineddine/ICIPCompet/DCCOCO/test/")


###Test dataset_train_val_split function from Preprocessing class
Preprocessing(images_path=image_paths,bboxs_path=bbox_paths).stratified_dataset_train_val_split(0.75,"/home/zeineddine/ICIPCompet/dataSplit/dataSplit4/train"\
                                                                                                ,"/home/zeineddine/ICIPCompet/dataSplit/dataSplit4/val"\
                                                                                                    ,with_resampling=True\
                                                                                                    ,resampling_array=resampling_weights\
                                                                                                        ,Transform=transform)
