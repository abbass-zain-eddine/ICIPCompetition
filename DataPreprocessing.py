from torch.utils.data import DataLoader, random_split
from DataLoad import DCCOCODataset
import os,shutil
class Preprocessing():
    def __init__(self,Dataset=None,images_path="",bboxs_path=""):
        self.Dataset=Dataset
        self.image_path=images_path
        self.bboxs_path=bboxs_path

    def set_Dataset(self,Dataset):
        self.Dataset=Dataset

    def set__image_and_bbox_paths(self,images_path,bboxs_path):
        self.image_path=images_path
        self.bboxs_path=bboxs_path        

    def dataset_split_train_val(self,percentageTrain=0.8):
        """
        this function split the data set randomly into train and validation datasets. it returns the train and validation dataset objects in addition to
        train and validation dataloaders
        """
        dataset_size = len(self.Dataset)
        train_size = int(percentageTrain * dataset_size)
        val_size = dataset_size - train_size
        train_dataset, val_dataset = random_split(self.Dataset, [train_size, val_size])
        batch_size = 32
        train_loader = DataLoader(train_dataset, batch_size=batch_size, shuffle=True,collate_fn=self.Dataset.collate_fn)
        val_loader = DataLoader(val_dataset, batch_size=batch_size, shuffle=False,collate_fn=self.Dataset.collate_fn)
        return train_loader,val_loader,train_dataset,val_dataset
    
    def stratified_dataset_train_val_split(self,percentageTrain,train_output_path,validation_output_path,with_resampling=False,resampling_array={},Transform=None):
        """this function calls the __stratified_dataset_train_val_split_calculation function and get its output then create training folder with the images and 
        labels for training and another folder for validation with the images and labels for validation 
        Note that the data is copied from its original path to the training and val folders and not moved."""
        if not os.path.exists(train_output_path):
            os.mkdir(train_output_path)
        if not os.path.exists(validation_output_path):
            os.mkdir(validation_output_path)
        try:
            os.mkdir(train_output_path+"/images")
            os.mkdir(train_output_path+"/labels")
            os.mkdir(validation_output_path+"/images")
            os.mkdir(validation_output_path+"/labels")
        except:
            pass
        train_image_paths,train_labels_paths,validation_image_paths,validation_labels_paths=self.__stratified_dataset_train_val_split_calculation(percentageTrain,with_resampling,resampling_array,Transform=Transform)
        for image,label in zip(validation_image_paths,validation_labels_paths):
            shutil.copy2(image,validation_output_path+"/images")
            shutil.copy2(label,validation_output_path+"/labels")
        i=0
        for image,label in zip(train_image_paths,train_labels_paths):
            if(os.path.exists(train_output_path+"/images/"+image.split('/')[-1])):
                shutil.copy2(image,train_output_path+"/images/"+str(i)+"_"+image.split('/')[-1])
                shutil.copy2(label,train_output_path+"/labels/"+str(i)+"_"+label.split('/')[-1])
                i+=1   
            else:         
                shutil.copy2(image,train_output_path+"/images")
                shutil.copy2(label,train_output_path+"/labels")


    def __stratified_dataset_train_val_split_calculation(self,percentageTrain,with_resampling=False,resampling_array={},Transform=None):
        """this function split the data into training and testing data set based on the number of instances of classes
        for exmaple if the percentage of training is 0.8, it is going to take 0.8 of each class and add it to the training 
        and the remaining 0.2 for testing. thus we will not have the problem of random splitting and each class will be represented in the 
        training and testing data set.
        In addition this function can be called with resampling True and a resampling array. in this case we can solve the problrm of
        imbalenced data. so the resampling array will contain the percentage of images we will take for each class from the total availabel images of the class.
        if the percentage is less than 1 then we are down sampling, and if it is greater than one then we are upsampling by consideing the image several times in 
        the training set.
        this function returns four lists. two list of the paths of trainig images and their corresponding labels, and two lists for the validation images paths and
        their corresponding labels. 
        """
        ds=DCCOCODataset(self.image_path,self.bboxs_path,transform=Transform)
        count, paths=ds.EDA(90)
        train_image_paths=[]
        train_labels_paths=[]
        validation_image_paths=[]
        validation_labels_paths=[]
        for class_number in sorted(count,key=count.get):
            if with_resampling:
                train_size=len(paths[class_number])*percentageTrain*resampling_array[class_number]
            else:
                train_size=len(paths[class_number])*percentageTrain
            i=0
            num_of_moved_paths=0
            array_of_paths_len=len(paths[class_number])
            current_paths=paths[class_number].copy()
            if resampling_array[class_number] > 1:
                #extending the array of pathes to include the image paths several times.
                extended_array_of_paths=current_paths[:int(array_of_paths_len*0.9)]*int((resampling_array[class_number]*array_of_paths_len)/(array_of_paths_len*0.9))
                array_of_paths_len=len(extended_array_of_paths)
                current_paths=extended_array_of_paths

            while i<train_size and i< array_of_paths_len:
                if current_paths[i] not in validation_image_paths and current_paths[i] not in train_image_paths:
                    train_image_paths.append(current_paths[i])
                    train_labels_paths.append('/'+'/'.join(self.bboxs_path[0].split('/')[:-2])+"/labels/"+current_paths[i].split('/')[-1][:-3]+"txt")
                    num_of_moved_paths+=1
                else:
                    train_size+=1
                i+=1
            if train_size>0:
                if with_resampling:
                    if resampling_array[class_number] < 1:
                        validation_image_paths.extend(current_paths[num_of_moved_paths:num_of_moved_paths+int(len(current_paths)*(1-percentageTrain)*resampling_array[class_number])])
                    else:
                        start=int(len(paths[class_number])*0.9)
                        p=paths[class_number]
                        validation_image_paths.extend(p[start:])
                else:
                    validation_image_paths.extend(current_paths[num_of_moved_paths:])

        if train_size>0:
            validation_labels_paths.extend(["/"+'/'.join(self.bboxs_path[0].split('/')[:-2])+"/labels/"+path.split('/')[-1][:-3]+"txt" for path in validation_image_paths])
        
        return train_image_paths,train_labels_paths,validation_image_paths,validation_labels_paths