from ultralytics import YOLO
# Create a data loader to load the dataset in batches
batch_size = 8
# Load a model
model = YOLO("yolov8m.yaml")  # build a new model from scratch
model = YOLO("/home/zeineddine/ICIPCompet/runs/detect/train85 25 epoch on split 2/weights/best.pt")  # load a pretrained model (recommended for training)
# Use the model
model.train(data="./yolo_config/myData 1.yaml",epochs=50,lr0=0.001,batch=batch_size,optimizer='Adam')  # train the model