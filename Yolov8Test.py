import torch
from torchvision import transforms
import numpy as np
import cv2
#import matplotlib.pyplot as plt
import glob
from ultralytics import YOLO
from DrawBoundingBoxes import   BboxPlot
# results would be a list of Results object including all the predictions by default
# but be careful as it could occupy a lot memory when there're many images, 
# especially the task is segmentation.
# 1. return as a list
model = YOLO("yolov8s.yaml")  # build a new model from scratch
model = YOLO("/home/zeineddine/ICIPCompet/runs/detect/train85/weights/best.pt")  # load a pretrained model (recommended for training) #
paths=glob.glob('/home/zeineddine/ICIPCompet/dataSplit/dataSplit3/val/images/*.jpg')
transform = transforms.Compose([
    transforms.Resize((640,640)),
    transforms.ToTensor(),
])
classes=['person','bicycle','car','motorcycle','airplane','bus','train','truck','boat','traffic light','fire hydrant','street sign'\
         ,'stop sign','parking meter','bench','bird','cat','dog','horse','sheep','cow','elephant','bear','zebra','giraffe','hat',
'backpack','umbrella','shoe','eye glasses','handbag','tie','suitcase','frisbee','skis','snowboard','sports ball','kite'\
    ,'baseball bat','baseball glove','skateboard','surfboard','tennis racket','bottle','plate','wine glass','cup','fork','knife'\
        ,'spoon','bowl','banana','apple','sandwich','orange','broccoli','carrot','hot dog','pizza','donut','cake','chair','couch'\
            ,'potted plant','bed','mirror','dining table','window','desk','toilet','door','tv','laptop','mouse','remote','keyboard'\
                ,'cell phone','microwave','oven','toaster','sink','refrigerator','blender','book','clock''vase','scissors','teddy bear'\
                    ,'hair drier','toothbrush','hair brush']
idx = 0

for image_path in paths:
        img = cv2.imread(image_path)
        predicted = model(img)
        BboxPlot(img,predicted[0].boxes.cpu().numpy()[:,[5,4,0,1,2,3]],classes).yolov8Plot(save_path="/home/zeineddine/ICIPCompet/dataSplit/dataSplit3/results",img_name=image_path.split('/')[-1])
